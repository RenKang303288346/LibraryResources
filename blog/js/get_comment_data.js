[
    {
        "parent": null,
        "id": 1,
        "content": "A",
        "author": {
            "username": "test"
        },
        "content_type": 16,
        "object_id": 1,
        "published_time": "2018-04-27T00:56:00",
        "n_like": 0,
        "n_dislike": 0,
        "is_spam": false,
        "children": [
            {
                "parent": 1,
                "id": 2,
                "content": "B",
                "author": {
                    "username": "debug"
                },
                "content_type": 19,
                "object_id": 1,
                "published_time": "2018-04-27T00:56:00",
                "n_like": 0,
                "n_dislike": 0,
                "is_spam": false,
                "children": [
                    {
                        "parent": 2,
                        "id": 5,
                        "content": "C",
                        "author": {
                            "username": "nimi"
                        },
                        "content_type": 19,
                        "object_id": 2,
                        "published_time": "2018-04-27T14:56:00",
                        "n_like": 0,
                        "n_dislike": 0,
                        "is_spam": false,
                        "children": [
                            {
                                "parent": 5,
                                "id": 6,
                                "content": "D",
                                "author": {
                                    "username": "ero"
                                },
                                "content_type": 19,
                                "object_id": 5,
                                "published_time": "2018-04-27T14:57:00",
                                "n_like": 0,
                                "n_dislike": 0,
                                "is_spam": false,
                                "children": []
                            }
                        ]
                    }
                ]
            },
            {
                "parent": 1,
                "id": 4,
                "content": "B1",
                "author": {
                    "username": "test"
                },
                "content_type": 19,
                "object_id": 1,
                "published_time": "2018-04-27T00:58:00",
                "n_like": 0,
                "n_dislike": 0,
                "is_spam": false,
                "children": []
            }
        ]
    },
    {
        "parent": null,
        "id": 3,
        "content": "A1",
        "author": {
            "username": "test"
        },
        "content_type": 16,
        "object_id": 1,
        "published_time": "2018-04-27T00:57:00",
        "n_like": 0,
        "n_dislike": 0,
        "is_spam": false,
        "children": []
    }
]